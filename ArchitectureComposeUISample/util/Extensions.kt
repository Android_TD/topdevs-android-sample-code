package com.arch.example.util

import kotlinx.coroutines.Job

inline val <reified T> T.TAG: String
    get() = T::class.java.name

val Job?.isActive: Boolean
    get() = this?.isActive == true